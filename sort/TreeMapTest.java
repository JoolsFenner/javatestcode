import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;


public class TreeMapTest{
	
	private void launch(){
		
		Map<Integer, String> map = new TreeMap<Integer, String>();
		map.put(80, "julian");
		map.put(1, "dave");
		map.put(1, "graciela");	
		map.put(2, "graciela");	
		map.put(4, "graciela");	
		map.put(5, "graciela");	
		map.put(2, "graciela");	
		
		for(Integer next : map.keySet()){
			System.out.println(next);
		}
	}

	public static void main(String[] args){
		new TreeMapTest().launch();
		
	}

}