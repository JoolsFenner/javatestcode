public class Launcher{

	private void launch(){
		Person p1 = new Person("Julian", 33);
		Person p2 = new Person("Linda", 32);
		Person p3 = new Person("Julian", 1);
		Person p4 = new Person("Julian", 33);
		
		System.out.println("Equals");
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p3));
		System.out.println(p1.equals(p4));
		System.out.println();
		System.out.println("Compare");
		System.out.println(p1.compareTo(p2));
		
	}

	public static void main(String[] args){
		new Launcher().launch();
	}
}