public class Question {
	private String question;
	private String[] answers;
	private int correctAnswer;
	
	public Question(String question){
		this.question = question;
		this.answers = new String[4]; 
	}
	
	public void setPotentialAnswer(int answerNumber, String answer){
		answerNumber -= 1; //because from userInput, zero indexed.
		answers[answerNumber] = answer;
	}
	
	public void setCorrectAnswer(int correctAnswer){
		this.correctAnswer = correctAnswer;
	}
	
	public String getQuestion(){
		return question;
	}
	
	public String getAnswers(){
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < answers.length; i++){
			builder.append("Answer " + (i+1) + ": " + answers[i] + "\r\n");
		}
		return builder.toString();
	
	}
	
	public int getCorrectAnswer(){
		return correctAnswer;
	}
	
}