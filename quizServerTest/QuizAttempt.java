public class QuizAttempt{
	private int quizId;
	private int quizScore;
	
	public QuizAttempt(int quizId, int quizScore){
		this.quizId = quizId;
		this.quizScore = quizScore;
	}
	
	public int getQuizScore(){
		return quizScore;
	}
	
	public int getquizId(){
		return quizId;
	}
}