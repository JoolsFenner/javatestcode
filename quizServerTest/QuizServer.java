import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class QuizServer extends UnicastRemoteObject implements QuizService{
	private List<Quiz> quizzes;
	private List<User> users;
	
	public QuizServer() throws RemoteException {
		this.quizzes = new ArrayList<Quiz>();
		this.users = new ArrayList<User>();
	}
		
	public void addQuiz(Quiz quiz) throws RemoteException{
		quizzes.add(quiz);
	}
	
	
	public List<Quiz> getQuizzes(){
		return quizzes;
	}
	
	public void createUser(String name, int id){
		User jools = new User(name, id);
		users.add(jools);
	}
	
	
	public void playQuiz(){
		Quiz q = quizzes.get(0);
		User u = users.get(0);
			
		Scanner scanner= new Scanner(System.in);
		
		
		List<Question> questions = q.getQuestions();;
		
		for(Question next : questions){
			System.out.println(next.getQuestion());
			System.out.println();
			System.out.println(next.getAnswers());
			
			System.out.print("Input answer: ");
			int userAnswer;
			boolean inputValid = false;
			
			do{
				//validate input
				char userInput = scanner.next().charAt(0);
				userAnswer = Character.getNumericValue(userInput);
				if(userAnswer == 1 || userAnswer == 2 || userAnswer == 3 || userAnswer == 4){
					inputValid = true;
				} else{
				//THROW EXCEPTION
					System.out.println("INVALID INPUT");
				}
			} while(!inputValid);
			
			//save results in userAttempt object because of multithreading
			System.out.println();
			if(next.getCorrectAnswer() == userAnswer){
				System.out.println("YOU ARE CORRECT");
			} else{
				System.out.println("YOU ARE WRONG");
			}
			
			
			System.out.println("______________");	
			System.out.println();	
			System.out.println();	
			
		}
	
	
	}
}