public class Person implements Comparable<Person>{
	
	private String name;
	private int age;
	
	public Person(String name, int age){
		this.name = name;
		this.age = age;
	}
	
	@Override
	public boolean equals (Object o){ 
		if(o == null) return false;
		
		if(! (o instanceof Person)) return false;
		
		Person other = (Person) o;
		
		return this.name.equals(other.name) && this.age == other.age;
		
		}
	
	public int compareTo(Person other){
		return this.name.compareTo(other.name);
	}
	
}