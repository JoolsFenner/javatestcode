public class AdultPerson implements Person{
	private String name;
	public AdultPerson (String name){
		this.name = name;
	}
	
	public void move(int distance){
		//implementation code goes here
	}
	
	public String getName(){
		return name;
	}
}