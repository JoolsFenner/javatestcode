import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface QuizService extends Remote {
	
	void addQuiz(Quiz quiz) throws RemoteException;
	
	List<Quiz> getQuizzes() throws RemoteException;
	 
	
	
}