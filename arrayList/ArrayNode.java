public class ArrayNode{
	private int n;
	private ArrayNode next;
	
	public ArrayNode(int n){
		this.n = n;
		this.next = null;
	}
	public void add(ArrayNode a){
		if(this.next == null){
			this.next = a;
		} else{
			this.next.add(a);
		}
	}
	
	public void printlist(){
		System.out.println(this);
		System.out.println(this.n);
		System.out.println(this.next);
		System.out.println();
		
		if(this.next != null){
			this.next.printlist();
		}
	}
	public ArrayNode getNext(){
		return next;
	}
	
	public void setNext(ArrayNode aux){
		this.next = aux;
	}
}