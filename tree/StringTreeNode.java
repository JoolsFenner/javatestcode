public class StringTreeNode{
	private String value;
	private StringTreeNode left;
	private StringTreeNode right;
	
	public StringTreeNode(String value){
		this.value = value;
		this.left = null;
		this.right = null;
	}
	
	public void add(String newValue){
	//add number to tree
	if(firstStringGreater(newValue, this.value)){
	// if (newValue > this.value) 
			if (right == null) {
				right = new StringTreeNode(newValue);
			} else {
				right.add(newValue);
			}
		} else {
			if (left == null) {
				left = new StringTreeNode(newValue);
			} else {
				left.add(newValue);
			}
		}
	}
	
	/*
	public void delete(String dataValue){
		StringTreeNode A, B, C, D;
		A = this;
		while(A != null && A.value != dataValue){
			B = A;
			if(firstStringGreater(dataValue, A.value)){
				A = A.right;
			} else{
				A = A.left;
			}	
		}
		
		if(A==null){
			System.out.println("not found");
		} else{
			// A points to the node to be deleted and B to its parent, unless A = Root.
			// Now find *C (perhaps nil) to replace *A, and prepare for linking *B to it.
			if(A.left==null){
				C = A.right;
			} else if (A.right == null){
				C = A.left;
			} else{
				// *A has two non-empty subtrees; so make C = insucc(A).
				// Note: insucc(A)"LLINK = nil.
				
				C=A.right;
				
				if(c.left !=null){
					do{
						D = C;
						C = C.left;
					} while (c.left != null);
				// Now link *C�s right subtree to *C�s parent *D.
					D.left = C.right;
					C.right = A.right;
				}
				C.left  = A.left; // *C inherits *A�s subtrees.
			}
			
			// Lastly, link *B to *C.
			
			if (A = Root) {
				Root  = C;
			} else if (B.LLINK == A){
				B.LLINK  = C;
			else {
				B.RLINK  = C;
			}
		
		
		
		
		
		
		
		}
	
	}
	
	*/
	
	
	
	
	
	/**
	 * compares two strings
	 *
	*/
	public boolean firstStringGreater (String firstString, String secondString){
		int compare = firstString.compareTo(secondString);
		
		if(compare < 0){
			return false;
			//-1, --> newString is less than currenString. ( newString comes alphabetically currenString)
		} else {
			return true; 
			 // ie.  compare > 0 then currenString comes alphabetically first.
		}
		//doesn't need to handle those examples where compare == 0 as BST won't have dupes
	}
	
	
	/**
	 * Print and Get Methods
	 *
	 *
	 */
	public void nodesToString(String headValue){
	//prints every node as a list in square brackets containing value, left branch and right branch
		if(left != null){
			left.nodesToString(headValue);
		}
		System.out.println("[ Node Value = " + this.value);
		System.out.println("  Left branch = " + getLeftValue()); 
		System.out.println("  Right branch = " + getRightValue());
		
		if(firstStringGreater(this.value, headValue)){
		
			System.out.print("  RIGHT TREE]");
		} else if(firstStringGreater(headValue, this.value)) {
			System.out.print("  LEFT TREE]");
		} else {
			System.out.print("  ROOT]");
		}
		System.out.println();
		System.out.println();
		
		if(right !=null){
			right.nodesToString(headValue);
		}
			
	}
	
	public String getValue(){
		return value;
	}
	
	
	public String getLeftValue(){
	//get this node's left value
		if(this.left!=null){
			return this.left.value;
		} 
		return null;	
	}
	
	public String getRightValue(){
	//get this node's right value
		if(this.right!=null){
			return this.right.value;
		} 
		return null;	
	}
	
	
	
	
}