public class SuperWriter {

	public SuperWriter(int n) {
		for (int i = 0; i < n; i++)
		System.out.print("N");
	}

	public SuperWriter() {
		System.out.print("W");
	}

	public static void main(String[] args) {
		System.out.println("String...");
		new Writer("ZYU");
		System.out.println();
		new SuperWriter(5);
		System.out.println();
	}

}