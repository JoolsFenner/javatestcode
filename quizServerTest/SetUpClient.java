import java.util.List;
import java.util.ArrayList;
import java.rmi.NotBoundException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.io.IOException;

//java -Djava.security.policy=client.policy QuizSetUpClient
public class SetUpClient {
	Remote service;
	QuizService quizService;
	
	public SetUpClient()throws NotBoundException, IOException{
		this.service = Naming.lookup("//127.0.0.1:1099/quiz");
		this.quizService = (QuizService) service;
	}
	
	private void launch() throws RemoteException{
		mainMenu();
	}
	
	
	public void createNewQuiz() throws RemoteException{
		System.out.println();
		System.out.println("***CREATE NEW QUIZ***");
		System.out.println();
		System.out.print("Enter quiz name: ");
		String quizName = System.console().readLine();	
		Quiz quiz = new Quiz(quizName);
		
		boolean finishedInputting = false;
		int questionNumber = 1;
		
		do{
			StringBuilder builder = new StringBuilder(); //TO DISPLAY USER INPUT AFTER EACH QUESTION.
			int answerNumber = 1;
			
			//GET QUESTION
			System.out.print("Enter question number " + questionNumber + ": "); 
			String question = System.console().readLine();
			builder.append("Question " + questionNumber + " " + question + "\r\n");
			questionNumber++;
			
			//GET ANSWERS
			System.out.print("Enter possible answer no " + answerNumber + ": ");
			String answer1 = System.console().readLine();
			builder.append("Answer " + answerNumber + " " + answer1 + "\r\n");
			answerNumber++;
			
			System.out.print("Enter possible answer no " + answerNumber + ": ");
			String answer2 = System.console().readLine();
			builder.append("Answer " + answerNumber + " " + answer2 + "\r\n");
			answerNumber++;
			
			System.out.print("Enter possible answer no " + answerNumber + ": ");
			String answer3 = System.console().readLine();
			builder.append("Answer " + answerNumber + " " + answer3 + "\r\n");
			answerNumber++;
			
			System.out.print("Enter possible answer no " + answerNumber + ": ");
			String answer4 = System.console().readLine();
			builder.append("Answer " + answerNumber + " " + answer4 + "\r\n");
			answerNumber++;
			
			//GET CORRECT ANSWER
			System.out.println();
			System.out.println(builder);
			System.out.print("Enter the number of the correct answer: ");
			int correctAnswer = Integer.parseInt(System.console().readLine());
			
			//ADD QUESTION AND ANSWERS TO QUIZ
			quiz.addQuestionAndAnswers(question, answer1, answer2, answer3, answer4, correctAnswer);
			
			String hasUserFinished;
			System.out.print("Have you finished? Enter y or any other key for no: ");
			hasUserFinished = System.console().readLine();
			if(hasUserFinished.equals("y")) finishedInputting = true;
			
		} while(!finishedInputting);
		
		//ADD QUIZ TO QUIZSERVER
		quizService.addQuiz(quiz);
		System.out.println("Quiz successfully added to quiz server"); // RETURN ID
		mainMenu();
		
	}
	
	
	public void printAllQuizzes() throws RemoteException{
		List<Quiz> quizList = quizService.getQuizzes();
		for (Quiz next : quizList){
			System.out.println(next);
		}
		mainMenu();
	}
	
	public void mainMenu() throws RemoteException{
		System.out.println();
		System.out.println("***MENU***");
		System.out.println("1 - Create new quiz");
		System.out.println("2 - View all quizzes");
		System.out.println();
		System.out.print("Please enter your option: ");
		
		
		int option;
		boolean inputValid = false;

		while(!inputValid){
			option = Integer.parseInt(System.console().readLine());
			
			switch(option){
				case 1:		inputValid = true;
							createNewQuiz();
							break;
				case 2:		printAllQuizzes();
							inputValid = true;
							break;
				default:	System.out.println("Invalid option");
							break;			
			}
		}
	}
	
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
		
		try{
			new SetUpClient().launch();
		} catch(IOException ex){
			System.out.println(ex);
		}
		/*
		//link client to server
		Remote service = Naming.lookup("//127.0.0.1:1099/quiz");
		QuizService quizService = (QuizService) service;
		*/
	}
}