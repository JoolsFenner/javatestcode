class Writer extends SuperWriter {
	public Writer(int num, boolean val, String s1, String s2) {
		super(num - 1);
		System.out.print(val ? s1 : s2);
	}

	public Writer() {
		System.out.print("I");
	}

	public Writer(String name) {
		System.out.println("A");
		System.out.println(name);
		new Writer();
		new Writer(2, (1 > 3), "BB", "CD");
	}

}