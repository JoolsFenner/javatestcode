import java.util.List;
import java.util.ArrayList;

public class Quiz{
	private String quizName;
	private List<Question> questions;
	
	public Quiz(String quizName){
		this.quizName = quizName;
		this.questions = new ArrayList<Question>();
	}
	
	public void addQuestionAndAnswers(String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer){
		Question q = new Question(question);
		q.setPotentialAnswer(1, answer1);
		q.setPotentialAnswer(2, answer2);
		q.setPotentialAnswer(3, answer3);
		q.setPotentialAnswer(4, answer4);
		q.setCorrectAnswer(correctAnswer);
		questions.add(q);
	}
	
	public List<Question> getQuestions(){
		return questions;
	}
	
		
	
	

			
}
	
	