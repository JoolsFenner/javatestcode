public class BoxLaunch{
	public static void main(String[] args){
		new BoxLaunch().launch();
	}
	
	private void launch(){
		/*Box<String> stringBox = new Box<String>("Julian");
		System.out.println(stringBox.boxGet());
		stringBox.boxSet("David");
		System.out.println(stringBox.boxGet());
		*/
		
		Person linda = new Person("Linda", 32);
		Box<Person> lindaBox = new Box<Person>(linda);
		
		System.out.println(lindaBox.boxGet().getName());
		
	}
}