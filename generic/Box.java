public class Box <T>{
	private T t;
	Box (T t){
		this.t =t;
	}
	
	public void boxSet(T t){
		this.t = t;
	}
	
	public T boxGet(){
		return t;
	}
}