/**
 * A node in a dynamic singly-linked list of Strings
 */
public class PersonQueueNode {
    private Person personInstance;
    private PersonQueueNode next;

    public PersonQueueNode(Person personInstance) {
	  this.personInstance = personInstance;
	  next = null;
    }

    /**
     * Returns the string in this node
     
    public String getText() {
	  return content;
    }*/

    /**
     * Set the next node to point to the given node
     */
    public void setNext(PersonQueueNode node) {
	  next = node;		
    }

    /**
     * Set the next node to point to the given node
     */
    public PersonQueueNode getNext() {
	  return next;		
    }
	
	public Person getPerson(){
		return personInstance;
	}
}