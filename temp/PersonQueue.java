public interface PersonQueue {
	/**
	* Adds another person to the queue.
	*/
	void insert(String name);
	/**
	* Removes a person from the queue.
	*/
	
	Person retrieve();
	
	
	//shows who's first and last in queue
	void printFirstLast();
	}