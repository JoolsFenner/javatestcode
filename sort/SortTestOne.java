import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class SortTestOne{


	private void launch(){
		List<String> unsortList = new ArrayList<String>();
		
		unsortList.add("CCC");
		unsortList.add("111");
		unsortList.add("AAA");
		unsortList.add("BBB");
		unsortList.add("ccc");
		unsortList.add("bbb");
		unsortList.add("aaa");
		unsortList.add("333");
		unsortList.add("222");
 
		//before sort
		System.out.println("ArrayList is unsorted");
		for(String temp: unsortList){
			System.out.println(temp);
		}
		
		System.out.println();
		System.out.println();
		
		//sort the list
		Collections.sort(unsortList);
 
		//after sorted
		System.out.println("ArrayList is sorted");
		for(String temp: unsortList){
			System.out.println(temp);
		}
		
	}

	public static void main(String[] args){
		new SortTestOne().launch();
	}

}